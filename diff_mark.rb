#!/usr/bin/env ruby

require 'redcarpet'
require 'kramdown'
require 'commonmarker'
require 'pry-byebug'
require 'optparse'
require 'pandoc-ruby'

COMMONMARK_OPTIONS = [:table, :tagfilter].freeze
REDCARPET_OPTIONS  = {
  fenced_code_blocks:  true,
  footnotes:           true,
  lax_spacing:         true,
  no_intra_emphasis:   true,
  space_after_headers: true,
  strikethrough:       true,
  superscript:         true,
  tables:              true
}.freeze
KRAMDOWN_OPTIONS = {
  input: 'GFM',
  smart_quotes: ["apos", "apos", "quot", "quot"],
  hard_wrap: false,
  syntax_highlighter: nil,
  gfm_quirks: "paragraph_end,no_auto_typographic"
}.freeze

OUTPUT_LOCATION           = "#{Dir.pwd}/diffs"
CMARK_OUTPUT_LOCATION     = 'commonmark'
YAML_REGEXP               = /\A----*(.|\n)*?----*/

class DocumentData
  attr_reader :source_text, :markdown_html, :markdown_converted_md,
              :commonmark_html, :commonmark_converted_md, :processor

  def initialize(source_text, processor: :redcarpet)
    @source_text = source_text
    @processor   = processor

    normalize_source
    processor == :redcarpet ? convert_redcarpet : convert_kramdown
    convert_commonmark
  end

  #------------------------------------------------------------------------------
  def equal?
    markdown_converted_md == commonmark_converted_md
  end

  #------------------------------------------------------------------------------
  def normalize_source
    # remove any YAML frontmatter
    @source_text.sub!(YAML_REGEXP, '')
  end

  #------------------------------------------------------------------------------
  def normalize_html
    # PLACEHOLDER
  end

  #------------------------------------------------------------------------------
  def markdown_output_location
    processor.to_s
  end

  #------------------------------------------------------------------------------
  def save_conversions(base_path, filename)
    base_path     = File.dirname(filename) unless base_path
    relative_path = Pathname.new(filename).relative_path_from(Pathname.new(base_path))
    markdown_path = File.join(OUTPUT_LOCATION, markdown_output_location, relative_path)
    cmark_path    = File.join(OUTPUT_LOCATION, CMARK_OUTPUT_LOCATION, relative_path)

    FileUtils.mkdir_p(File.dirname(markdown_path))
    FileUtils.mkdir_p(File.dirname(cmark_path))

    File.write("#{markdown_path}.original.md", source_text)
    File.write("#{markdown_path}.converted.md", markdown_converted_md)
    File.write("#{markdown_path}.converted.html", PandocRuby.convert(markdown_converted_md, from: :commonmark, to: :html))
    File.write("#{markdown_path}.raw_converted.html", markdown_html)

    File.write("#{cmark_path}.original.md", source_text)
    File.write("#{cmark_path}.converted.md", commonmark_converted_md)
    File.write("#{cmark_path}.converted.html", PandocRuby.convert(commonmark_converted_md, from: :commonmark, to: :html))
    File.write("#{cmark_path}.raw_converted.html", commonmark_html)
  end

  #------------------------------------------------------------------------------
  def save_in_place(filename)
    File.write(filename, markdown_converted_md)
  end

  private

  #------------------------------------------------------------------------------
  def convert_redcarpet
    redcarpet               = Redcarpet::Markdown.new(Redcarpet::Render::HTML.new, REDCARPET_OPTIONS)
    @markdown_html          = redcarpet.render(source_text)
    @markdown_converted_md  = PandocRuby.convert(markdown_html, from: :html, to: :commonmark)
  end

  #------------------------------------------------------------------------------
  def convert_kramdown
    kramdown                = Kramdown::Document.new(source_text, KRAMDOWN_OPTIONS)
    @markdown_html          = kramdown.to_html
    @markdown_converted_md  = PandocRuby.convert(markdown_html, from: :html, to: :commonmark)
  end

  #------------------------------------------------------------------------------
  def convert_commonmark
    @commonmark_html         = CommonMarker.render_doc(source_text, :FOOTNOTES, COMMONMARK_OPTIONS).to_html
    @commonmark_converted_md = PandocRuby.convert(commonmark_html, from: :html, to: :commonmark)
  end
end

#------------------------------------------------------------------------------
def parse_options
  options = {}
  option_parser = OptionParser.new do |opts|
    executable_name = File.basename($PROGRAM_NAME)
    opts.banner = <<-BANNER

Generates differences between processing Markdown files with RedCarpet and CommonMark

Usage: #{executable_name} [options] [file or directory]
BANNER

    opts.on('--convert', 'Convert to CommonMark in-place, overwriting existing files') do
      options[:convert] = true
    end

    opts.on('--html', 'Output rendered HTML of differing documents') do
      options[:output_html] = true
    end

    opts.on('--kramdown', 'Process Markdown with kramdown instead of RedCarpet') do
      options[:kramdown] = true
    end

    opts.on('-v','--verbose', 'Lists successes and failures') do
      options[:verbose] = true
    end

    opts.on('-h', '--help', 'Show this help') do
      options[:help] = true
    end
  end
  option_parser.parse!

  if options[:help]
    puts option_parser.help
    exit(0)
  end

  return options
end

#------------------------------------------------------------------------------
def console_input_to_file
  puts "Input Markdown to convert then press ctrl-d\n\n"
  source = STDIN.read

  file_path = File.expand_path('diffs/from_console.md')
  FileUtils.mkdir_p(File.dirname(file_path))
  File.write(file_path, source)
  file_path
end

#------------------------------------------------------------------------------
def ensure_pandoc_installed!
  PandocRuby.convert('# Test Pandoc', from: :markdown, to: :html)
rescue
  puts 'It looks like Pandoc is not installed.'
  puts 'Please visit http://pandoc.org/installing.html.'
  exit(-1)
end

#------------------------------------------------------------------------------

ensure_pandoc_installed!

options         = parse_options
path            = ARGV.shift
path            = path ? File.expand_path(path) : nil

if path
  base_path = Dir.exist?(path) ? path : File.dirname(path)
  files     = Dir.exist?(path) ? Dir.glob(path + '/**/*.md') : [path]
else
  file_path = console_input_to_file
  base_path = Dir.exist?(file_path) ? path : File.dirname(file_path)
  files     = [file_path]
end

success = failed = 0

files.each do |file|
  source = File.read(file)
  data   = DocumentData.new(source, processor: options[:kramdown] ? :kramdown : :redcarpet)
  data.normalize_html
  if data.equal?
    success += 1
    puts "+  #{file}" if options[:verbose]
  else
    failed += 1
    puts "-  #{file}"
    data.save_conversions(base_path, file) if options[:output_html]
    data.save_in_place(file) if options[:convert]
  end
end

puts '------------------------------------------------------------------------------'
puts "Rendered HTML is identical: #{success}"
puts "Rendered HTML is different: #{failed}"
